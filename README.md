# eStore
A full-featured, Bootstrap-based Drupal Theme — built with all the must-have features of an online store in mind — designed to meet any e-commerce site project's (specific) needs.

## Ahead of the game:  Commerce Gateway
Your [Drupal Commerce (v2) Docs](https://docs.drupalcommerce.org/commerce2)  Store will need to [Commerce Gateway](https://docs.drupalcommerce.org/commerce2/developer-guide/payments).  There is a wide range of Commerce Gateway Modules you will need to decide around:
- [Commerce Gateway Documentation](https://docs.drupalcommerce.org/commerce2/developer-guide/payments/available-gateways)
- [Drupal Modules | Search for "commerce gateway"](https://www.drupal.org/project/project_module?f%5B0%5D=&f%5B1%5D=&f%5B2%5D=&f%5B3%5D=&f%5B4%5D=sm_field_project_type%3Afull&f%5B5%5D=&f%5B6%5D=&text=commerce+gateway&solrsort=score+desc&op=Search)


## PHP Requirements
PHP requirements are always given by Drupal Core itself.  Versions of Commerce (Commerce Core) do also have PHP specification in their Composer.json.  Please be aware of this in your environments.  (Unix) Terminal, PHP and Composer setups, guides and helpful information can be found in their respective community pages.  Individual sites can have a wide range of complication with setups.
- [PHP requirements | Drupal.org](https://www.drupal.org/docs/getting-started/system-requirements/php-requirements)
- [Commerce | drupal project page](https://www.drupal.org/project/commerce (https://git.drupalcode.org/project/commerce/-/blob/8.x-2.x/composer.json?ref_type=heads))

## Composer Codebase Setup Instructions
It is now only possible to install with Composer as Commerce Core and Address modules both require other PHP Libraries to be included.

## Composer Requirements
- Composer 2.x is needed for all 2.x and 3.x eStore websites (PHP 8+)
- Composer 1.x is needed with and 8.x-1.x sites as this is normal for this generation (PHP 7.x era)

## Composer & PHP Key Tips
I. You should expect to specific in your composer ‘config’ your PHP Version using this sample:
```json
"platform": {"php": “8.0"},
 ```
Otherwise:
 ```shell
composer install --ignore-platform-reqs
```
The param ‘--ignore-platform-reqs' will bypass some of the complexity in running a build.  Be aware that this can in fact effect the versions of libraries being downloaded and installed by composer.  Please follow the Documentation appropriate for your own project setup.

II. Composer Minimum Stability Config
```json
"minimum-stability": "dev"
```
Some modules (such as [inline_entity_form | Drupal project page](https://www.drupal.org/project/inline_entity_form))* have longstanding ‘dev’ only releases.  It may be necessary for you to set your composer stability downward this way.  Be sure you are aware and conformable with Contributed Module and PHP workflows in this way!
*At this time (Nov 2023) IFE Module is reaching RC’s!

III. PHP ensure you have enough PHP Memory, runtime/timeout settings (as well as apache/server timeouts) in control.  Be aware of 'input vars' with PHP [(core and contrib) Interfaces can go over max_input_vars](https://www.drupal.org/project/drupal/issues/1565704)

## Terminal Codebase Install Commands
Composer will shortly build 2 basic folder 'web' and 'vendor'.  Web is for you Server Web Head to be pointed to.  Vendor a part of Security Hardening.  For the Drupal version you will run your site by select one of these commands to build the required modules for the Your eStore codebase:

- #### Drupal 8 Base (legacy. no longer supported):
```shell
composer create-project drupal/recommended-project:^8.9 my_estore_site_name
cd my_estore_site_name
composer require drupal/estore:^1.17
```

- #### Drupal 9 Base:
```shell
composer create-project drupal/recommended-project:^9.3 my_estore_site_name
cd my_estore_site_name
composer require drupal/estore:^2.0
```

- #### Drupal 10 Base:
```shell
composer create-project drupal/recommended-project:^10 my_estore_site_name
cd my_estore_site_name
composer require drupal/estore:^2.1
```

> Tip:  List all estore versions (from base directory, with projects composer.json in place)
> ```shell
> composer show "drupal/estore" --all
> ```

- #### Add your Commerce Gateway modules:  
  Select the Commerce Gateway modules ("Ahead of the Game" section above!) and use their composer commands to add them to your codebase.  Follow the instructions of those tools if any...

## Setup your "Drupal Libraries" in your codebase
1. Download and install the <a href="https://github.com/twbs/bootstrap/releases/download/v3.3.7/bootstrap-3.3.7-dist.zip" rel="nofollow">Bootstra-3.3.7 ZIP</a>&nbsp;the Bootstrap library and extract it into the "web/libraries/bootstrap" folder of your codebase.
2. Download and install the <a href="http://johnpolacek.github.io/stacktable.js/" rel="nofollow">Stacktable.js</a> library.  <a href="https://github.com/johnpolacek/stacktable.js/zipball/master/" rel="nofollow">Stacktable.js | github</a>&nbsp;Stacktable.js and extract int into the "web/libraries/stacktable.js" folder of your codebase.

## Install your Drupal Site.
- [x] In a Web Browser, go to your host or local environment with your web browser using apache/server (with htaccess setup). Example:  [https://estore.local](https://estore.local)
- [x] Run Drupal's first install of  your codebase.   Follow the steps of the installer: Select the ’**Standard Profile**’.
- [x] Enable the following list of modules on the Drupal Extend page at /admin/modules
  - Views Bootstrap 4 [views_bootstrap]
  - Commerce
    - commerce_product
    - commerce_cart
    - commerce_order
    - commerce_store
    - commerce_tax
    - commerce_payment
  - Commerce Wishlist [commerce_wishlist]
  - pathauto
  - block_class
  - Font Awesome [fontawesome]
  - Panels
  - Page Manager [page_manager]
    - Page Manager UI [page_manager_ui]
  - admin_toolbar
    - admin_toolbar_tools
    - admin_toolbar_search
    - admin_toolbar_links_access_filter
>   Other Core Modules
>   - Inline Form Errors [inline_form_errors] will enhance all Form Error Messaging across your Store and website
- [x] Verify to /admin/config/content/fontawesome in your site and make sure the select list for "**Font Awesome Method**" is set to "**Web Fonts with CSS**".
- [x] The eStore theme has some theme settings you will want to edit for the site’s Header and Footers: **admin/appearance/settings/estore**
- [x] **Enable the homepage Feature**
Go to the Modules list page (/admin/modules) and enable the "eStore homepage feature" this module will provide a homepage.  Go to /admin/config/system/site-information here verify  "**Default front page**” is set to ’/home'
> Tip:  Page Manager UI can be found: admin/structure/page_manager. Page Manger is a toolkit for building other such Landing Pages!
- [x] **Your Store**: Go to admin/commerce/config/stores and setup your Default Store. See [Drupal Commerce (v2) Docs](https://docs.drupalcommerce.org/commerce2)
- [x] Setup your [Commerce Gateway](https://docs.drupalcommerce.org/commerce2/developer-guide/payments/available-gateways) as required.
- [x] As you need:  Configure the **Product type**. You can Manage form display page of the Default Product type (/admin/commerce/config/product-types/default/edit/form-display).  This is the general admin area to customize your products and other content in Drupal!
- [x] Build the content for the Products, Frontpage Slider..
- [x] Enjoy your site!


## Other Mentions
Below are some Other Topic you maybe looking for!

### eStore Theme:  Block Overview
-----------------------------
The Regions (for Layout, Block placement) the eStore theme provides can be seen at: /admin/structure/block/demo/estore

### Configuration
--------------
You will find your site Configuration Management here: admin/config/development/configuration
Deeper Reading:  [Drupal Configuration Management](https://www.drupal.org/docs/administering-a-drupal-site/configuration-management).

### Your Menus
-----------
Your sites Menu’s can be found from:  /admin/structure/menu

### Translation
------------
The eStore does not enable Drupal’s Content and Configuration Translation.  You can simple enable these Modules and continue the setup/configuation for your specific needs. /admin/modules/ Multilingual Category.

### Search
-------
Of course, Drupal Core’s ‘Search’ module is the base for the site Search box and ready out of the box!

### Cron
-----
Go to: /admin/config/system/cron and review your server setup to run cron.


## MAINTAINERS
  * [OPTASY](https://www.optasy.com) is a Canadian Drupal development & web development company based in Toronto. In the past we provided Drupal development solutions for a variety of Canadian and foregin companies with outstanding results.
  * [web247](https://www.drupal.org/u/web247) (Adrian Ababei) Drupal Project Owner.
